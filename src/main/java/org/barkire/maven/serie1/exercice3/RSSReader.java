package org.barkire.maven.serie1.exercice3;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class RSSReader {
	
	public RSSReader(){
		
	}
	
	public static InputStream read(String url){
		InputStream reponse=null;
		
		// Ouverture d'un client
		HttpClient httpClient= HttpClientBuilder.create().build();
		
		// Création d'une méthode HTTP
		HttpGet get= new HttpGet(url);
		
		try {
			HttpResponse response= httpClient.execute(get);
			StatusLine statusLine= response.getStatusLine();
			
			int statusCode= statusLine.getStatusCode();
			if(statusCode!= HttpStatus.SC_OK){
				System.err.println("Method failed: "+ statusLine);
			}
		
		// Lecture de la reponse dans un flux
		reponse = response.getEntity().getContent();
			
	
	}
	catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reponse;
	}
}
