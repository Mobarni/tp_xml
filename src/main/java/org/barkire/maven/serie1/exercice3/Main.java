package org.barkire.maven.serie1.exercice3;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.events.Attribute;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		
		String url="https://www.voxxed.com/feed";
	
		// Informations renvoyées pas l'URL
		InputStream contenu= RSSReader.read(url);
		
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance() ;
		saxParserFactory.setValidating(true) ;      
		saxParserFactory.setNamespaceAware(true) ;                               
		
		/**
		 *  Handler permettant d'obtenir le nom de l'élément racine
		 */
		DefaultHandler handler= new DefaultHandler(){
			boolean test=true;
			public void startDocument(){
				System.out.println(" *** Début de l'analyse du document *** ");
				
			}
		
			public void startElement(String namespaceURI, String lname,	String qname, Attributes attrs) throws SAXException {
				if(test){
				System.out.println("Le nom de l'élément racine = " + lname);
				test=false;
				}
			}
			

			public void endElement(String uri, String localName, String qName) throws SAXException {
				
			}
			
			public void endDocument(){
				System.out.println("Fin: Détermination du noeud racine");
				
			}
			
		};
		
		/**
		 *  Handler permettant d'obtenir le nombre de sous-éléments de l'élément racine
		 */
		DefaultHandler handler2= new DefaultHandler(){
			int count;
			boolean test=true;
			String rootName;
			public void startDocument(){
				System.out.println("Début comptage des éléments du noeud racine... ");
				
			}
		
			public void startElement(String namespaceURI, String lname,	String qname, Attributes attrs) throws SAXException {
				if(test){
					rootName=lname;
					test=false;
				}
				else{	
					count++;
				}
			}
			
			public void endElement(String uri, String localName, String qName) throws SAXException {
				
			}
			
			public void endDocument(){
				System.out.println("Fin: Le nombre de sous éléments de l'élément racine est:"+ count);
			}
			
		};
		
		/**
		 *  Handler permettant d'obtenir le nombre d'éléments item
		 */
		DefaultHandler handler3= new DefaultHandler(){
			int count3;
			public void startDocument(){
				System.out.println("Début comptage des éléments item du document... ");
			}
		
			public void startElement(String namespaceURI, String lname,	String qname, Attributes attrs) throws SAXException {
				if(qname.equals("item")){
					count3++;
				}
			}
			
			public void endElement(String uri, String localName, String qName) throws SAXException {
			
			}
			
			public void endDocument(){
				System.out.println("Fin: Le nombre d'élément item de ce flux est:"+ count3);
			}
		};
		/**
		 * Handler permettant d'obtenir dans une liste les titres des éléments item
		 */
		DefaultHandler handler4= new DefaultHandler(){
			List<String> listTitles=new ArrayList<String>();
			boolean inItem=false;
			boolean inTitle=false;
			public void startDocument(){
				System.out.println("Début de recherche des titres des item... ");
			}
		
			public void startElement(String namespaceURI, String lname,	String qname, Attributes attrs) throws SAXException {
				
				
				if(qname.equals("item")){
					inItem=true;
				}
				if(inItem &&qname.equals("title")){
					inTitle=true;
				}
				
			}
			public void characters(char[] caracteres, int debut, int longueur) {
				if(inTitle)
				{
					String title = new String(caracteres, debut, longueur);
					listTitles.add(title);
					
				}
			}
			public void endElement(String uri, String localName, String qName) throws SAXException {
			
					inItem=false;
					inTitle=false;
			
			}
			
			public void endDocument(){
				System.out.println("Fin: les titres des items sont:"+ listTitles);
			}
		};
		/**
		 * Handler permettant d'obtenir dans une liste de toute les catégories commencant par 
		 * http://www.java.net/topic/
		 */	
	DefaultHandler handler5=new DefaultHandler(){
		String  nomItem=null;
		boolean inTitle = false;
		boolean inTitleCategory = false;
		List<String> itemTitlesCategory= new ArrayList<String>(); 

		public void startElement(String uri, String localName,String qName, Attributes attributes){

			if (qName.equals("item")){
			nomItem = qName;
			}

			if (nomItem != null){
				if (qName.equals("title")){
					inTitle = true;
				}

				if (qName.equals("category")){
					inTitleCategory = true;
				}
			}
		}

		public void endElement(String uri, String localName,String qName){

			if (qName.equals(nomItem)){
				nomItem = null;
			}
		}

		public void characters (char ch[], int start, int length){
			String contenu = new String(ch, start, length); 


			if (inTitleCategory&& inTitle){
				if (contenu.startsWith("http://home.java.net/topic/")){
				itemTitlesCategory.add(contenu);
				inTitleCategory = false;
				}
			}
		}

		public void endDocument(){
				System.out.println(itemTitlesCategory);
			}
	};

		

	

		try {				 
			SAXParser saxParser = saxParserFactory.newSAXParser() ;
			saxParser.parse(RSSReader.read(url), handler);
			saxParser.parse(RSSReader.read(url), handler2);
			saxParser.parse(RSSReader.read(url), handler3);
			saxParser.parse(RSSReader.read(url), handler4);
			saxParser.parse(RSSReader.read(url), handler5);
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

