package org.barkire.maven.serie1.exercice1_2;

import java.io.File;
import java.io.IOException;

import org.dom4j.Document;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Marin m= new Marin(1234, "MAIGA", "Ide", 50);
		Marin M= new Marin();
		Document d;
		
		d=XMLUtil.serialize(m);
		File xmlFile= new File("marin.xml");
		try {
			// Ecriture dans un document XML
			XMLUtil.write(d, xmlFile);
			
			// Construction d'un objet à partir d'un document
			M= XMLUtil.deserialize(d);
			
			// Affichage du nouvel object
			System.out.println(M);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
