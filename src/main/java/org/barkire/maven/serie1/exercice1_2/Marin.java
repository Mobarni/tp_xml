package org.barkire.maven.serie1.exercice1_2;

public class Marin {
	private long id;
	private String nom, prenom;
	private int age;
	/**
	 * Le constructeur vide
	 */
	public  Marin(){
		
	}
	
	/**
	 * Le deuxième constructeur
	 * @param newId
	 * @param newNom
	 * @param newPrenom
	 * @param newAge
	 */
	public  Marin(long newId, String newNom, String newPrenom, int newAge){
		this.id= newId;
		this.nom= newNom;
		this.prenom= newPrenom;
		this.age= newAge;	
	}
	
	/**
	 * Méthode qui retourne l'id du marin
	 * @return
	 */
	public long getId() {
		return id;
	}

	/**
	 * Méthode qui définit l'id du marin
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Méthode qui retourne le nom du marin
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Méthode qui définit le nom du marin
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Méthode qui retourne le prénom du marin
	 * @return
	 */
	public String getPrenom() {
		return prenom;
	}
	
	/**
	 * Méthode qui définit l'id du marin
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Méthode qui retourne l'age du marin
	 * @return
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Méthode qui définit l'age du marin
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (age != other.age)
			return false;
		if (id != other.id)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Marin [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]";
	}
	
	
	
}
