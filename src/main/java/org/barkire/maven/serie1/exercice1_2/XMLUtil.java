package org.barkire.maven.serie1.exercice1_2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter; 

public class XMLUtil {
	
	/**
	 * Méthode qui permet de retourner un document xml à partir d'un objet
	 * @param m
	 * @return
	 */
	public  static Document serialize(Marin m){
		// Exercice 1
		// 3.a.  On crée une instance de cette classe à l'aide de la
		// méthode static createDocument de la classe DocumentHelper
		// qui retourne un objet de type document
		Document d = DocumentHelper.createDocument();
		
		// 3.b. Le noeud racine de notre document se nomme root
		Element root= d.addElement("marin");
		// 3.c. Le rajout d'un noeud fils se fait en appliquant la méthode addElement
		// à l'objet root
		// 3.d. Le rajout d'un attribut à un noeud racine se fait
		// en appliquant la methode addAttribute au noeud root
		root.addAttribute("id", Long.toString(m.getId()));
		
		Element nom1= root.addElement("nom");
		
		// 3.e. Le rajout d'un noeud d'un contenu textuel à un noeud XML se
		// fait en appliquant la méthode addText au noeud
			nom1.addText(m.getNom());
		Element prenom1=root.addElement("prenom");
			prenom1.addText(m.getPrenom());
		Element age1=root.addElement("age");
			age1.addText(Integer.toString(m.getAge()));
			
		return d;
		
	}
	
	/**
	 * Méthode permettant d'écrire un document xml dans un fichier
	 * @param d
	 * @param fichier
	 * @return
	 * @throws IOException
	 */
	public static boolean write(Document d, File fichier) throws IOException{
		if(d==null||fichier==null){
			return false;
		}
		else{
			
			try {
				FileOutputStream fos= new FileOutputStream(fichier);
				OutputFormat format= OutputFormat.createPrettyPrint();
				XMLWriter writer = new XMLWriter(fos, format);
				writer.write(d);
				writer.close();
			
			} 
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			return true;
		}	
	}	
	
	/**
	 * Méthode permettant de créer un document xml à partir d'un fichier
	 * @param fichier
	 * @return
	 * @throws DocumentException
	 * @throws FileNotFoundException
	 */
	public static Document read(File fichier) throws DocumentException, FileNotFoundException{
		Document d=null;
		SAXReader reader = new SAXReader();
		 d= reader.read(fichier);
		return d;
	}
	
	/**
	 * Méthode permettant de créer un objet à partir d'un document xml
	 * @param d
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Marin deserialize(Document d){

		Marin m= new Marin();
	// Exercice 2
	// 2.a. La façon de connaître le nom d'un élément est d'appliquer la méthode
	// getName()
	// 2.b. Pour savoir qu'un élément à des attributs, on applique la méthode attributes 
	// pour récupérer la liste des attribues
	// 3.c. On obtient la valeur de cette attribut en appliquant la méthode getValue
	// 3.d On obtient la liste des sous-éléments d'un élement en appliquant la méthode elements
	// à ce noeud
	// 3.e On connaît le contenu d'un élément avec la méthode getValue	
 		Element root= d.getRootElement();
 		List<Attribute> attr= root.attributes();
 		int id= Integer.parseInt(attr.get(0).getValue());
		m.setId(id);
		List<Element> elements= root.elements();
		Element elt= elements.get(0);
		Element elt2= elements.get(1);
		Element elt3= elements.get(2);
		m.setNom(elt.getStringValue());
		m.setPrenom(elt2.getStringValue());
		m.setAge(Integer.parseInt(elt3.getStringValue()));
		
		return m;	
	}
}
